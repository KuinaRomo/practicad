var notes = {};

exports.list = function(req, res) {
    res.json(Object.keys(notes));     
}
exports.get = function(note_name, req, res) {
    if (!notes.hasOwnProperty(note_name)) {
        res.status(404);
        res.end();
        console.error(note_name + " NOT FOUND!");
    } else {
        res.json(notes[note_name]);
    }
};

exports.insert = function(note_name, req, res) {
    if (notes.hasOwnProperty(note_name)) {
        res.status(409);
        res.end();
        return console.error(note_name + " ALREADY THERE!");
    } 
    else {
        if (!req.body.content) {
            res.status(422);
            res.end();
            return console.error(note_name + " INCORRECT BODY JSON!");
        }
        notes[note_name] = { content: req.body.content, inserted: new Date() };
        res.end();
    }
};


exports.upsert = function(note_name, req, res) {
    if (notes.hasOwnProperty(note_name)) {
        notes[note_name] = { content: req.body.content, modified: new Date() };
        res.end();
    } else {
        res.status(201);
        this.insert(note_name, req, res);
        console.log(Object.keys(notes));
    }
};

exports.delete = function(note_name, req, res) {
    if(notes.hasOwnProperty(note_name)) {
        delete notes[note_name];
        res.end();
    }

    else {
        res.status(404);
        res.end();
        return console.error(note_name + " DOESN'T EXISTS");
    }
}

exports.search = function(search, req, res) {
    var anyText = false;
    console.log(req.body.search);
    if (!req.body.search) {
        res.status(422);
        res.end();
        return console.error(search + " INCORRECT BODY JSON!");
    }
    var names = Object.keys(notes);
    console.log(names);
    var jsonText = [];
    for(let i = 0; i < names.length; i++){
        if(notes[names[i]].content.search(search) != -1){
            jsonText.push(notes[names[i]].content);
            anyText = true;
        } 
    }
    if(!anyText){
        res.status(404);
        res.end();
        return console.error(search + " THERE AREN'T ANY COMMENT WITH THIS STRING!");
    }
    res.json(jsonText);
}